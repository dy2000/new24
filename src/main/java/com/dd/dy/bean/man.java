package com.dd.dy.bean;

import java.io.Serializable;
/*
* DY
* 记录最近实现登录的人名
* */
public class man implements Serializable {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "man{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
