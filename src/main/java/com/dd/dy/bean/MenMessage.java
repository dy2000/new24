package com.dd.dy.bean;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
/*
* 所有用户信息实体类
* */
public class MenMessage implements Serializable {
    private int menId;
    private String telephone;
    private int qq;
    private String address;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date day;
    private user user;

    public int getMenId() {
        return menId;
    }

    public void setMenId(int menId) {
        this.menId = menId;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public int getQq() {
        return qq;
    }

    public void setQq(int qq) {
        this.qq = qq;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public com.dd.dy.bean.user getUser() {
        return user;
    }

    public void setUser(com.dd.dy.bean.user user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "MenMessage{" +
                "menId=" + menId +
                ", telephone='" + telephone + '\'' +
                ", qq=" + qq +
                ", address='" + address + '\'' +
                ", name='" + name + '\'' +
                ", day=" + day +
                ", user=" + user +
                '}';
    }
}
