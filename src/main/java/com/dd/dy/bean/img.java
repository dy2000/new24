package com.dd.dy.bean;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
/*
* DY
* 展品实体类
* */
public class img implements Serializable {
    private Integer id;
    private String img;
    private String value;
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "img{" +
                "id=" + id +
                ", img='" + img + '\'' +
                ", value='" + value + '\'' +
                ", file=" + file +
                '}';
    }
}
