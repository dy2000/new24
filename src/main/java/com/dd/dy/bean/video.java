package com.dd.dy.bean;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
/*
* DY
* 视频实体类
* */
public class video implements Serializable {
    private int id;
    private String newname;
    private String value;
    private String img;
    private String video;
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewname() {
        return newname;
    }

    public void setNewname(String newname) {
        this.newname = newname;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    @Override
    public String toString() {
        return "video{" +
                "id=" + id +
                ", newname='" + newname + '\'' +
                ", value='" + value + '\'' +
                ", img='" + img + '\'' +
                ", video='" + video + '\'' +
                ", file=" + file +
                '}';
    }
}
