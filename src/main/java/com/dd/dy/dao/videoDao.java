package com.dd.dy.dao;

import com.dd.dy.bean.video;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface videoDao {
    @Select("select * from video")
    List<video> list();

    @Insert("insert into video(newname,value,img,video) values(#{newname},#{value},#{img},#{video})")
    void addvideo(video v);

    @Delete("delete from video where id = #{id}")
    boolean deleteById(int id);
}
