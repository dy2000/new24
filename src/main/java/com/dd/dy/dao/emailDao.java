package com.dd.dy.dao;

import com.dd.dy.bean.email;
import com.dd.dy.bean.img;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface emailDao {
    @Select("select * from email")
    List<email> list();

    @Insert("insert into email(title,con,name,email) values(#{title},#{con},#{name},#{email})")
    void addEmail(email e);

    @Delete("delete from email where id = #{id}")
    boolean deleteById(int id);
}
