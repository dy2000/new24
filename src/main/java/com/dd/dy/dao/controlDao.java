package com.dd.dy.dao;

import com.dd.dy.bean.control;
import org.apache.ibatis.annotations.Select;

import java.util.*;
public interface controlDao {
    @Select("select * from control where name=#{name}")
    control findByName(String name);
}
