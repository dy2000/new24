package com.dd.dy.dao;

import com.dd.dy.bean.img;
import com.dd.dy.bean.video;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface imgDao {
    @Select("select * from img")
    List<img> list();

    @Insert("insert into img(img,value) values(#{img},#{value})")
    void addImg(img m);

    @Delete("delete from img where id = #{id}")
    boolean deleteById(int id);
}
