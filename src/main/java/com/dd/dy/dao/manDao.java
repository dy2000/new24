package com.dd.dy.dao;

import com.dd.dy.bean.MenMessage;
import com.dd.dy.bean.man;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface manDao {
    @Update("update man set name=#{name}")
    void updateMan(String name);
    @Select("select name from man where id =1")
    String selectName();
}
