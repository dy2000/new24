package com.dd.dy.dao;

import com.dd.dy.bean.img;
import com.dd.dy.bean.log;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface logDao {
    @Select("select * from log")
    List<log> list();

    @Insert("insert into log(name,method,q,time) values(#{name},#{method},#{q},#{time})")
    void addlog(log g);
}