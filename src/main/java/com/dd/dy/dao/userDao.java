package com.dd.dy.dao;

import com.dd.dy.bean.MenMessage;
import com.dd.dy.bean.user;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public interface userDao {
    @Select("select * from user")
    List<user> findAll();

    @Select("select * from user where username=#{username}")
    user findByUserName(String username);

    @Insert("insert into user(username,password) values(#{username},#{password})")
    boolean add(user user);

    @Insert("insert into MenMessage(name,telephone,address,qq,day) values(#{name},#{telephone},#{address},#{qq},#{day})")
    boolean addMen(MenMessage men);

    @Select("select * from MenMessage where menId = #{menId}")
    MenMessage findMen(int menId);

    @Select("select * from MenMessage")
    List<MenMessage> findAllMen();

    @Delete("delete from user where username = #{username} and password = #{password}")
    boolean deleteById(user user);

    @Select("select * from user where id =#{id}")
    user selectById(int id);

    @Select("select * from user where username =#{username}")
    user selectByname(String username);

    @Select("select * from MenMessage where name =#{name}")
    MenMessage selectMenByName(String name);

    @Update("update user set password=#{password} where username =#{username}")
    void update(user user);

    @Update("update MenMessage set telephone=#{telephone},address=#{address},qq=#{qq},day=#{day} where menId =#{menId}")
    void updateMen(MenMessage men);

    @Select("select * from user where name = ${username}")
    boolean UniqueName(String username);

    @Delete("delete from MenMessage where menId = #{menId}")
    boolean deletemenById(int menId);

}
