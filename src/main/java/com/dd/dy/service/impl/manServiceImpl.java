package com.dd.dy.service.impl;


import com.dd.dy.bean.man;
import com.dd.dy.dao.manDao;
import com.dd.dy.service.manService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class manServiceImpl implements manService {

    @Autowired
    private manDao man;

    @Override
    public void updateMan(String name) {
        man.updateMan(name);
    }

    @Override
    public String selectName() {
        return man.selectName();
    }
}
