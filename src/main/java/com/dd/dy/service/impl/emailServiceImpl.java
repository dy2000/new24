package com.dd.dy.service.impl;

import com.dd.dy.bean.email;
import com.dd.dy.dao.emailDao;
import com.dd.dy.service.emailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class emailServiceImpl implements emailService {
    @Autowired
    private emailDao emailDao;
    @Override
    public List<email> list() {
        return emailDao.list();
    }

    @Override
    public void addEmail(email e) {
        emailDao.addEmail(e);
    }

    @Override
    public boolean deleteById(int id) {
        return emailDao.deleteById(id);
    }
}
