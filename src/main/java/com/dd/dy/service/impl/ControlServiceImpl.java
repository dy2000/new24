package com.dd.dy.service.impl;

import com.dd.dy.bean.control;
import com.dd.dy.bean.log;
import com.dd.dy.bean.man;
import com.dd.dy.bean.user;
import com.dd.dy.dao.controlDao;
import com.dd.dy.dao.logDao;
import com.dd.dy.dao.manDao;
import com.dd.dy.service.ControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service("controlService")
public class ControlServiceImpl implements ControlService {
    @Autowired
    private controlDao ControlDao;
    @Autowired
    private logDao logDao;
    @Autowired
    private manDao manDao;
    @Override
    public int login(String name, String pwd) {
        control con = ControlDao.findByName(name);
        if (con!=null&&con.getPwd().equals(pwd)){
            log g = new log();
            g.setName(con.getName());
            g.setMethod("登录");
            g.setQ(1);
            Calendar calendar= Calendar.getInstance();
            g.setTime(calendar.getTime());
            logDao.addlog(g);
            manDao.updateMan(con.getName());
            return con.getId();
        }
        return -1;
    }
}
