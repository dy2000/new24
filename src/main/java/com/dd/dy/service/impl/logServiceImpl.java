package com.dd.dy.service.impl;

import com.dd.dy.bean.log;
import com.dd.dy.dao.logDao;
import com.dd.dy.service.logService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class logServiceImpl implements logService {
    @Autowired
    private logDao logdao;

    @Override
    public List<log> list() {
        return logdao.list();
    }

    @Override
    public void addLog(log g) {
        logdao.addlog(g);
    }
}
