package com.dd.dy.service.impl;

import com.dd.dy.bean.video;
import com.dd.dy.dao.videoDao;
import com.dd.dy.service.videoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class videoServiceImpl implements videoService {
    @Autowired
    private videoDao vd;
    @Override
    public List<video> list() {
        return vd.list();
    }

    @Override
    public void addvideo(video v) {
        vd.addvideo(v);
    }

    @Override
    public boolean deleteById(int id) {
        return vd.deleteById(id);
    }
}
