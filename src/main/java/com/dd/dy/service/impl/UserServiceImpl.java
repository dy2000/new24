package com.dd.dy.service.impl;

import com.dd.dy.bean.MenMessage;
import com.dd.dy.bean.log;
import com.dd.dy.bean.man;
import com.dd.dy.bean.user;
import com.dd.dy.dao.logDao;
import com.dd.dy.dao.manDao;
import com.dd.dy.dao.userDao;
import com.dd.dy.service.UserService;
import com.dd.dy.service.manService;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private userDao userDao;
    @Autowired
    private logDao logDao;
    @Autowired
    private manDao manDao;

    @Override
    public int login(user User) {
        user user = userDao.findByUserName(User.getUsername());
        if (user!=null&&user.getPassword().equals(User.getPassword())){
            log g = new log();
            g.setName(user.getUsername());
            g.setMethod("登录");
            g.setQ(0);
            Calendar calendar= Calendar.getInstance();
            g.setTime(calendar.getTime());
            logDao.addlog(g);
            manDao.updateMan(user.getUsername());
            return user.getId();
        }
        return -1;
    }

    @Override
    public List<user> findAll() {
        return userDao.findAll();
    }

    @Override
    public boolean add(user user) {
        return userDao.add(user);
    }

    @Override
    public boolean addMen(MenMessage men) {
        return userDao.addMen(men);
    }

    @Override
    public boolean deleteById(user user) {
        return userDao.deleteById(user);
    }

    @Override
    public user selectUserById(int id) {
        return userDao.selectById(id);
    }

    @Override
    public user selectUserByname(String username) {
        return userDao.selectByname(username);
    }

    @Override
    public MenMessage selectMenByName(String name) {
        return userDao.selectMenByName(name);
    }

    @Override
    public void update(user user) {
        userDao.update(user);
    }

    @Override
    public void updateMen(MenMessage men) {
        userDao.updateMen(men);
    }

    @Override
    public boolean UniqueName(String username) {
        return false;
    }

    @Override
    public List<user> list() {
        return userDao.findAll();
    }

    @Override
    public MenMessage findmen(int menId) {
        return userDao.findMen(menId);
    }

    @Override
    public List<MenMessage> findAllmen() {
        return userDao.findAllMen();
    }

    @Override
    public boolean deletemenById(int menId) {
        return userDao.deletemenById(menId);
    }

    @Override
    public void exportExcel(List<MenMessage> menList, OutputStream excelOutStream) throws IOException {
        HSSFWorkbook workbook=new HSSFWorkbook();
        //合并单元格
        CellRangeAddress cellRangeAddress=new CellRangeAddress(0,0,0,6);
        //创建表
        HSSFSheet sheet=workbook.createSheet("用户");
        HSSFRow nameRow=sheet.createRow(0);
        nameRow.createCell(0).setCellValue("用户表");
        //第一行设置为标题
        sheet.addMergedRegion(cellRangeAddress);
        //第二行设置字段名
        HSSFRow titleRow=sheet.createRow(1);
        String[] fieldName={
                "名字","电话","地址","qq","注册日期"};
        for(int i=0;i<fieldName.length;i++){

            titleRow.createCell(i).setCellValue(fieldName[i]);
        }
        int account=menList.size();
        for(int i=0;i<account;i++){

            MenMessage everStudent=menList.get(i);
            HSSFRow row=sheet.createRow(i+2);
            row.createCell(0).setCellValue(everStudent.getName());
            row.createCell(1).setCellValue(everStudent.getTelephone());
            row.createCell(2).setCellValue(everStudent.getAddress());
            row.createCell(3).setCellValue(everStudent.getQq());
            row.createCell(4).setCellValue(everStudent.getDay());
        }
        //响应给前台
        workbook.write(excelOutStream);
    }

}
