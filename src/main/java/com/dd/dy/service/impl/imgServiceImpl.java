package com.dd.dy.service.impl;

import com.dd.dy.bean.img;
import com.dd.dy.dao.imgDao;
import com.dd.dy.service.imgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class imgServiceImpl implements imgService {
    @Autowired
    private imgDao img;
    @Override
    public List<img> list() {
        return img.list();
    }

    @Override
    public void addImg(img m) {
        img.addImg(m);
    }

    @Override
    public boolean deleteById(int id) {
        return img.deleteById(id);
    }
}
