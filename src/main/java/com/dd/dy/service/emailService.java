package com.dd.dy.service;

import com.dd.dy.bean.email;
import com.dd.dy.bean.img;

import java.util.List;

public interface emailService {
    List<email> list();

    public void addEmail(email e);

    boolean deleteById(int id);
}
