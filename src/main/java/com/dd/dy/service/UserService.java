package com.dd.dy.service;

import com.dd.dy.bean.MenMessage;
import com.dd.dy.bean.user;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public interface UserService {
    int login(user User);

    List<user> findAll();

    boolean add(user user);

    boolean addMen(MenMessage men);

    boolean deleteById(user user);

    user selectUserById(int id);

    user selectUserByname(String username);

    MenMessage selectMenByName(String name);

    void update(user user);

    void updateMen(MenMessage men);

    boolean UniqueName(String username);

    public List<user> list();

    public MenMessage findmen(int menId);

    public List<MenMessage> findAllmen();

    boolean deletemenById(int menId);

    public void exportExcel(List<MenMessage> menList, OutputStream excelOutStream)throws IOException;
}
