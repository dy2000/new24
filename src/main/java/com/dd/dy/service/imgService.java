package com.dd.dy.service;

import com.dd.dy.bean.img;
import com.dd.dy.bean.video;

import java.util.List;

public interface imgService {
    List<img> list();

    public void addImg(img m);

    boolean deleteById(int id);
}
