package com.dd.dy.controller;

import com.dd.dy.bean.log;
import com.dd.dy.bean.video;
import com.dd.dy.service.impl.videoServiceImpl;
import com.dd.dy.service.logService;
import com.dd.dy.service.manService;
import com.dd.dy.service.videoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Calendar;
import java.util.UUID;

@Controller
@RequestMapping("/video")
public class videoController {

    @Autowired
    private videoServiceImpl videoServiceimp;
    @Autowired
    private videoService service;
    @Autowired
    private logService logService;
    @Autowired
    private manService manService;

    //执行上传视频封面并跳转回上传界面，方便继续上传
    @RequestMapping("/upload")
    public String upload(video v, HttpServletRequest request, Model model) throws Exception{
        System.out.println(request.getParameter("newname"));
        //保存数据库的路径
        String sqlPath = null;
        //定义文件保存的本地路径
        String localPath="D:\\java\\ideaf\\new24\\src\\main\\webapp\\images\\";
        //定义 文件名
        String filename=null;
        if(!v.getFile().isEmpty()){
            //生成uuid作为文件名称
            String uuid = UUID.randomUUID().toString().replaceAll("-","");
            //获得文件类型（可以判断如果不是图片，禁止上传）
            String contentType=v.getFile().getContentType();
            //获得文件后缀名
            String suffixName=contentType.substring(contentType.indexOf("/")+1);
            //得到 文件名
            filename=uuid+"."+suffixName;
            System.out.println(filename);
            //文件保存路径
            v.getFile().transferTo(new File(localPath+filename));
        }
        //把图片的相对路径保存至数据库
        sqlPath = "/images/"+filename;
        System.out.println(sqlPath);
        //user.setId(1);
        v.setNewname(request.getParameter("newname"));
        v.setValue(request.getParameter("value"));
        v.setVideo(request.getParameter("video"));
        v.setImg(sqlPath);
        service.addvideo(v);
        model.addAttribute("v", v);
        log g = new log();
        g.setName(manService.selectName());
        g.setMethod("上传了视频封面");
        logService.addLog(g);
        return "new_input";
    }

    //跳转到上传视频界面
    @RequestMapping("/b1")
    public String b1(){
        return "new_input";
    }

    //跳转到显示所有视频详细信息页面
    @RequestMapping("/b2")
    public String b2(){
        return "new_allvideo";
    }

    //执行删除视频功能
    @RequestMapping(value = "/deleteBtn", method = {RequestMethod.POST,RequestMethod.GET})
    public String deleteBtn(@RequestParam("id") int id) {
        service.deleteById(id);
        log g = new log();
        g.setName(manService.selectName());
        g.setMethod("删除了视频资源");
        Calendar calendar= Calendar.getInstance();
        g.setTime(calendar.getTime());
        logService.addLog(g);
        return "redirect:/imglist2";
    }
}
