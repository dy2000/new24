package com.dd.dy.controller;

import com.dd.dy.bean.email;
import com.dd.dy.bean.log;
import com.dd.dy.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Calendar;

@Controller
@RequestMapping("/main")
public class mainController {
    @Autowired
    private emailService emailService;
    @Autowired
    private manService manService;
    @Autowired
    private logService logService;

    //跳转到登录页面
    @RequestMapping(value = "/login",method = {RequestMethod.POST,RequestMethod.GET})
    public String login(){
        return "login";
    }

    //跳转到博物馆首页
    @RequestMapping(value = "/main1",method = {RequestMethod.POST,RequestMethod.GET})
    public String main() {
        return "main";
    }

    //跳转到博物馆新闻页面
    @RequestMapping(value = "/news",method = {RequestMethod.POST,RequestMethod.GET})
    public String news(){
        return "new24_news";
    }

    //跳转到博物馆商品页面
    @RequestMapping(value = "/show",method = {RequestMethod.POST,RequestMethod.GET})
    public String show(){
        return "show";
    }

    //跳转到博物馆展览页面
    @RequestMapping(value = "/email",method = {RequestMethod.POST,RequestMethod.GET})
    public String email(){
        return "new_email";
    }

    //调到博物馆提交建议页面
    @RequestMapping(value = "/putemail",method = {RequestMethod.POST,RequestMethod.GET})
    public String putemail(){
        return "new_inputEmail";
    }

/*
    @RequestMapping(value = "/deleteBtn", method = {RequestMethod.POST,RequestMethod.GET})
    public String deleteBtn(@RequestParam("id") int id) {
        emailService.deleteById(id);
        log g = new log();
        g.setName(manService.selectName());
        g.setMethod("删除了展览物品图片");
        Calendar calendar= Calendar.getInstance();
        g.setTime(calendar.getTime());
        logService.addLog(g);
        return "redirect:/emailList";
    }*/

    //执行博物馆提交建议并调到博物馆提交建议页面
    @RequestMapping(value = "/puttoemail",method = {RequestMethod.POST,RequestMethod.GET})
    public String putToEmail(email e){
        emailService.addEmail(e);
        return "new_inputEmail";
    }

    //执行意见删除功能
    @RequestMapping(value = "/deleteBtn1", method = {RequestMethod.POST,RequestMethod.GET})
    public String deleteBtn1(@RequestParam("id") int id) {
        emailService.deleteById(id);
        log g = new log();
        g.setName(manService.selectName());
        g.setMethod("删除了一条建议");
        Calendar calendar= Calendar.getInstance();
        g.setTime(calendar.getTime());
        logService.addLog(g);
        return "redirect:/emailList1";
    }

}
