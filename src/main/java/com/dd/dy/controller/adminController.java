package com.dd.dy.controller;

import com.dd.dy.bean.MenMessage;
import com.dd.dy.bean.user;
import com.dd.dy.service.ControlService;
import com.dd.dy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin")
public class adminController {
    @Autowired
    private UserService userService;
    @Autowired
    private ControlService controlService;

//跳转到用户更新页面
    @RequestMapping(value = "/toUpdateBook", method = {RequestMethod.POST,RequestMethod.GET})
    public String updateBtn(int menId, Model model) {
        MenMessage findmen = userService.findmen(menId);
        model.addAttribute("men",findmen);//将查询对象设置到视图模型中
        return "new24_selectMenByName";
    }

//删除用户信息并跳转回查询所有用户信息界面
    @RequestMapping(value = "/deleteBtn", method = {RequestMethod.POST,RequestMethod.GET})
    public String deleteBtn(@RequestParam("menId") int menId) {
        userService.deletemenById(menId);
        return "redirect:/list";
    }

    //跳转到添加用户界面
    @RequestMapping(value = "/addBtn", method = {RequestMethod.POST,RequestMethod.GET})
    public String addBtn() {
        return "demo";
    }

    //更新用户信息并跳转回查询所有用户信息界面
    @RequestMapping(value = "/update", method = {RequestMethod.POST,RequestMethod.GET})
    public String updateBook(MenMessage menMessage)
    {
        userService.updateMen(menMessage);
        return "redirect:/list";
    }

    //跳转到根据id更新用户界面
    @RequestMapping(value = "/update1", method = {RequestMethod.POST,RequestMethod.GET})
    public String update1() {
        return "new_selectid";
    }
}
