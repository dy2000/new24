package com.dd.dy.controller;

import com.dd.dy.bean.*;
import com.dd.dy.service.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@Controller
public class pageController {
    @Autowired
    UserService userService;
    @Autowired
    videoService service;
    @Autowired
    imgService imgService;
    @Autowired
    logService logService;
    @Autowired
    emailService emailService;

    //跳转实现用户详细信息分页显示
    @RequestMapping(value = "/list",method = {RequestMethod.POST,RequestMethod.GET})
    public String pageList(ModelMap map, @RequestParam(defaultValue="1",required=true,value="pageNo") Integer pageNo){
        int pageSize=5;//每页显示记录数
        //分页查询
        PageHelper.startPage(pageNo, 5);
        List<MenMessage> userList = userService.findAllmen();//获取所有用户信息
        System.out.println(userList);
        PageInfo<MenMessage> pageInfo=new PageInfo<MenMessage>(userList);
        map.addAttribute("pageInfo", pageInfo);

        return "fen";
    }

    //跳转实现博物馆视频分页显示
    @RequestMapping(value = "/imglist",method = {RequestMethod.POST,RequestMethod.GET})
    public String imgList(ModelMap map, @RequestParam(defaultValue="1",required=true,value="pageNo") Integer pageNo){
        int pageSize=5;//每页显示记录数
        //分页查询
        PageHelper.startPage(pageNo, 5);
        List<video> v = service.list();//获取所有用户信息
        System.out.println(v);
        PageInfo<video> pageInfo = new PageInfo<>(v);
        map.addAttribute("v", pageInfo);

        return "new_video";
    }

    //跳转实现视频详细信息分页显示
    @RequestMapping(value = "/imglist2",method = {RequestMethod.POST,RequestMethod.GET})
    public String imgList2(ModelMap map, @RequestParam(defaultValue="1",required=true,value="pageNo") Integer pageNo){
        int pageSize=5;//每页显示记录数
        //分页查询
        PageHelper.startPage(pageNo, 5);
        List<video> v = service.list();//获取所有用户信息
        System.out.println(v);
        PageInfo<video> pageInfo = new PageInfo<>(v);
        map.addAttribute("v", pageInfo);

        return "new_allvideo";
    }

    //跳转实现展品详细信息分页显示
    @RequestMapping(value = "/imglist1",method = {RequestMethod.POST,RequestMethod.GET})
    public String imgList1(ModelMap map, @RequestParam(defaultValue="1",required=true,value="pageNo") Integer pageNo){
        int pageSize=5;//每页显示记录数
        //分页查询
        PageHelper.startPage(pageNo, 5);
        List<img> m = imgService.list();//获取所有用户信息
        System.out.println(m);
        PageInfo<img> pageInfo = new PageInfo<>(m);
        map.addAttribute("m", pageInfo);

        return "new_allimg";
    }

    //跳转实现日志详细信息分页显示
    @RequestMapping(value = "/loglist",method = {RequestMethod.POST,RequestMethod.GET})
    public String logList(ModelMap map, @RequestParam(defaultValue="1",required=true,value="pageNo") Integer pageNo){
        int pageSize=5;//每页显示记录数
        //分页查询
        PageHelper.startPage(pageNo, 5);
        List<log> g = logService.list();//获取所有用户信息
        System.out.println(g);
        PageInfo<log> pageInfo = new PageInfo<>(g);
        map.addAttribute("g", pageInfo);

        return "new_alllog";
    }

/*    //跳转实现博物馆建议详细信息分页显示
    @RequestMapping(value = "/emailList",method = {RequestMethod.POST,RequestMethod.GET})
    public String emailList(ModelMap map, @RequestParam(defaultValue="1",required=true,value="pageNo") Integer pageNo){
        int pageSize=5;//每页显示记录数
        //分页查询
        PageHelper.startPage(pageNo, 5);
        List<email> e = emailService.list();//获取所有用户信息
        System.out.println(e);
        PageInfo<email> pageInfo = new PageInfo<>(e);
        map.addAttribute("e", pageInfo);

        return "new_email";
    }*/

    //跳转实现建议详细信息分页显示
    @RequestMapping(value = "/emailList1",method = {RequestMethod.POST,RequestMethod.GET})
    public String emailList1(ModelMap map, @RequestParam(defaultValue="1",required=true,value="pageNo") Integer pageNo){
        int pageSize=5;//每页显示记录数
        //分页查询
        PageHelper.startPage(pageNo, 5);
        List<email> e = emailService.list();//获取所有用户信息
        System.out.println(e);
        PageInfo<email> pageInfo = new PageInfo<>(e);
        map.addAttribute("e", pageInfo);

        return "new_allemail";
    }

}