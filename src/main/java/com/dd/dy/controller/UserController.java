package com.dd.dy.controller;

import com.dd.dy.bean.MenMessage;
import com.dd.dy.bean.control;
import com.dd.dy.bean.log;
import com.dd.dy.bean.user;
import com.dd.dy.service.ControlService;
import com.dd.dy.service.UserService;
import com.dd.dy.service.logService;
import com.dd.dy.service.manService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private ControlService controlService;
    @Autowired
    private manService manService;
    @Autowired
    private logService logService;

    //实现用户登录
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public String login(@Valid user user, BindingResult result,HttpSession session){
        if (result.hasErrors()){
            return "login";
        }
       int fu = userService.login(user);

        return "main";
    }

    //实现管理员登录
    @RequestMapping(value = "/controllogin",method = RequestMethod.POST)
    public String controllogin(@RequestParam("name") String name,@RequestParam("pwd") String pwd){
        int fu = controlService.login(name,pwd);
        if (fu!=-1){
            return "new_back";
        }else{
            return "loginnonull";
        }
    }

    //实现管理员登录验证
    @RequestMapping(value = "/demo",method = RequestMethod.POST)
    public String demo(MenMessage men){
        if(men.getUser().getUsername().equals(men.getName())){
            if (userService.UniqueName(men.getName())){
                return "namenosame";
            }
            boolean fol = userService.addMen(men);
            userService.add(men.getUser());
            if (fol) {
                return "new_back";
            }else{
                return "unsuccess";
            }
        }else{
            return "namenosame";
        }
    }

    //跳转到注册页面
    @RequestMapping(value = "/btn",method = RequestMethod.POST)
    public String btn(){
        log g = new log();
        g.setName(manService.selectName());
        g.setMethod("更新");
        Calendar calendar= Calendar.getInstance();
        g.setTime(calendar.getTime());
        logService.addLog(g);
        return "demo";
    }
    //跳转到管理员登录页面
    @RequestMapping(value = "/btn1",method = RequestMethod.POST)
    public String btn1(){
        return "control";
    }

/*    //跳转到管理页面
    @RequestMapping(value = "/gl    ",method = RequestMethod.POST)
    public String btn2(){
        return "new_back";
    }*/

    @RequestMapping(value = "/out",method = {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public void exportExcel(HttpServletResponse response) throws IOException {


        //获取导出的学生
        List<MenMessage> allStudent = userService.findAllmen();
        //把下载文件的日期添加到文件名上
        SimpleDateFormat date=new SimpleDateFormat("yyyy-MM-dd HH:mmss");
        String excelName=date+"账户金额.xls";
        //响应 导出Excel
        response.setHeader("Content-Disposition","attachment;filename="+excelName);
        response.setContentType("application/x-excel;charset=UTF-8");
        OutputStream excelOutStream=response.getOutputStream();
        if(allStudent!=null){

            userService.exportExcel(allStudent,excelOutStream);
        }
        excelOutStream.close();
    }
}
