package com.dd.dy.controller;

import com.dd.dy.bean.img;
import com.dd.dy.bean.log;
import com.dd.dy.bean.video;
import com.dd.dy.service.imgService;
import com.dd.dy.service.impl.imgServiceImpl;
import com.dd.dy.service.impl.videoServiceImpl;
import com.dd.dy.service.logService;
import com.dd.dy.service.manService;
import com.dd.dy.service.videoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Calendar;
import java.util.UUID;

@Controller
@RequestMapping("/img")
public class imgController {
    @Autowired
    private imgServiceImpl imgServiceimpl;
    @Autowired
    private imgService imgService;
    @Autowired
    private com.dd.dy.service.logService logService;
    @Autowired
    private com.dd.dy.service.manService manService;

//执行上传图片并跳转回上传界面，方便继续上传
    @RequestMapping("/upload")
    public String upload(img m, HttpServletRequest request, Model model) throws Exception {
        System.out.println(request.getParameter("value"));
        //保存数据库的路径
        String sqlPath = null;
        //定义文件保存的本地路径
        String localPath = "D:\\java\\ideaf\\new24\\src\\main\\webapp\\images\\";
        //定义 文件名
        String filename = null;
        if (!m.getFile().isEmpty()) {
            //生成uuid作为文件名称
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            //获得文件类型（可以判断如果不是图片，禁止上传）
            String contentType = m.getFile().getContentType();
            //获得文件后缀名
            String suffixName = contentType.substring(contentType.indexOf("/") + 1);
            //得到 文件名
            filename = uuid + "." + suffixName;
            System.out.println(filename);
            //文件保存路径
            m.getFile().transferTo(new File(localPath + filename));
        }
        //把图片的相对路径保存至数据库
        sqlPath = "/images/" + filename;
        System.out.println(sqlPath);
        //user.setId(1);
        m.setValue(request.getParameter("value"));
        m.setImg(sqlPath);
        imgService.addImg(m);
        model.addAttribute("v", m);
        log g = new log();
        g.setName(manService.selectName());
        g.setMethod("上传了展览物品图片");
        logService.addLog(g);
        return "new_inputimg";
    }

    //跳转到上传图片界面
    @RequestMapping("/b1")
    public String b1(){
        return "new_inputimg";
    }

    //根据id删除展览信息并跳转回显示全部展品页面
    @RequestMapping(value = "/deleteBtn", method = {RequestMethod.POST,RequestMethod.GET})
    public String deleteBtn(@RequestParam("id") int id) {
        imgService.deleteById(id);
        log g = new log();
        g.setName(manService.selectName());
        g.setMethod("删除了展览物品图片");
        Calendar calendar= Calendar.getInstance();
        g.setTime(calendar.getTime());
        logService.addLog(g);
        return "redirect:/imglist1";
    }
}
