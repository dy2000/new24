<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>注册页面</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../../css/new.css" type="text/css">
    <link rel="stylesheet" href="../../css/new_back.css" type="text/css">
  </head>
  <body>



  <div class="wrap">
  </div>
  <div class="nav-main">
    <div class="nav-box">
      <div class="nav">
        <ul class="nav-ul">
          <li><a href="#" class="home"><span>首页</span></a></li>
          <li><a href="#" class="develop"><span>用户管理</span></a></li>
          <li><a href="#" class="wechat"><span>视频管理</span></a></li>
          <li><a href="#" class="case"><span>展品管理</span></a></li>
          <li><a href="#" class="news"><span>日志管理</span></a></li>
          <li><a href="#" class="contact"><span>关于我们</span></a></li>
        </ul>
      </div>
      <div class="nav-slide">
        <div class="nav-slide-o"></div>
        <div class="nav-slide-o">
          <ul>
            <li><a href="${pageContext.request.contextPath}/list"><span>用户查询</span></a></li>
            <li><a href="${pageContext.request.contextPath}/admin/update1"><span>用户修改</span></a></li>
            <li><a href="${pageContext.request.contextPath}/admin/addBtn"><span>用户添加</span></a></li>
          </ul>
        </div>
        <div class="nav-slide-o">
          <ul>
            <li><a href="${pageContext.request.contextPath}/imglist2"><span>视频查询</span></a></li>
            <li><a href="${pageContext.request.contextPath}/video/b1"><span>视频提交</span></a></li>
          </ul>
        </div>
        <div class="nav-slide-o">
          <ul>
            <li><a href="${pageContext.request.contextPath}/imglist1"><span>显示展品</span></a></li>
            <li><a href="${pageContext.request.contextPath}/img/b1"><span>添加展品</span></a></li>
          </ul>
        </div>
        <div class="nav-slide-o">
          <ul>
            <li><a href="${pageContext.request.contextPath}/loglist"><span>查看所有日志</span></a></li>
          </ul>
        </div>
        <div class="nav-slide-o">
          <ul>
            <li><a href="${pageContext.request.contextPath}/emailList1">显示反馈<span></span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script>
  <script type="text/javascript">
    $(function(){
      var thisTime;
      $('.nav-ul li').mouseleave(function(even){
        thisTime	=	setTimeout(thisMouseOut,1000);
      })

      $('.nav-ul li').mouseenter(function(){
        clearTimeout(thisTime);
        var thisUB	=	$('.nav-ul li').index($(this));
        if($.trim($('.nav-slide-o').eq(thisUB).html()) != "")
        {
          $('.nav-slide').addClass('hover');
          $('.nav-slide-o').hide();
          $('.nav-slide-o').eq(thisUB).show();
        }
        else{
          $('.nav-slide').removeClass('hover');
        }

      })

      function thisMouseOut(){
        $('.nav-slide').removeClass('hover');
      }

      $('.nav-slide').mouseenter(function(){
        clearTimeout(thisTime);
        $('.nav-slide').addClass('hover');
      })
      $('.nav-slide').mouseleave(function(){
        $('.nav-slide').removeClass('hover');
      })

    })
  </script>


    <div class="container-fluid" id="play">
      <div class="row">
        <div class="col-lg-2"></div>
        <h3 class="col-lg-5">欢迎注册</h3>
      </div>

    <form class="form-horizontal form1" action="/user/demo" method="post">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="form-group col-lg-5">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="输入用户名">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3"></div>
      <div class="form-group col-lg-5">
        <label for="username">Username</label>
        <input type="text" class="form-control" id="username" name="user.username" placeholder="再次输入用户名">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3"></div>
      <div class="form-group col-lg-5">
        <label for="password">Password</label>
        <input type="text" class="form-control" id="password" name="user.password" placeholder="密码">
      </div>
      </div>

      <div class="row">
        <div class="col-lg-3"></div>
      <div class="form-group col-lg-5">
        <label for="telephone">Telephone</label>
        <input type="text" class="form-control" id="telephone" name="telephone" placeholder="电话">
      </div>
    </div>

      <div class="row">
        <div class="col-lg-3"></div>
        <div class="form-group col-lg-5">
          <label for="day">birthday</label>
          <input type="text" class="form-control" id="day" name="day" placeholder="请输入出生日期">
        </div>
      </div>

    <div class="row">
      <div class="col-lg-3"></div>
      <div class="form-group col-lg-5">
        <label for="qq">QQ</label>
        <input type="text" class="form-control" id="qq" name="qq" placeholder="QQ">
      </div>
    </div>

    <div class="row">
      <div class="col-lg-3"></div>
      <div class="form-group col-lg-5">
        <label for="address">Address</label>
        <input type="text" class="form-control" id="address" name="address" placeholder="地址">
      </div>
    </div>

    <div class="row">
      <div class="col-lg-3"></div>
      <div class="form-group col-lg-3">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </div>
    </form>
  </div>
  </body>
</html>