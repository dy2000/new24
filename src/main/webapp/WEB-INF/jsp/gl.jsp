<%--
  Created by IntelliJ IDEA.
  User: dy
  Date: 2021/5/21
  Time: 8:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../js/jquery-3.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../css/gl.css" type="text/css">
</head>
<body>
<h3>请选择需要执行的管理</h3>
    <form action="/list" method="post" id="fen">
        <input type="submit" value="查看用户注册信息" id="fena1" class="btn btn-info">
    </form>
    <form action="/admin/toUpdateBook" method="post" id="new24_updateMen">
        <input type="submit" value="修改用户信息" id="new24_updateMen1" class="btn btn-info">
    </form>
    <form action="/video/b1" method="post" id="new24_deleteMen">
        <input type="submit" value="删除用户信息" id="new24_deleteMen1" class="btn btn-info">
    </form>
    <form action="/admin/addBtn" method="post" id="new24_addMen">
        <input type="submit" value="添加新的用户信息" id="new24_addMen1" class="btn btn-info">
    </form>
</body>
</html>
