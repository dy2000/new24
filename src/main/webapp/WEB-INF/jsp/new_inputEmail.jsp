<%--
  Created by IntelliJ IDEA.
  User: dy
  Date: 2021/6/22
  Time: 22:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>上传意见</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../js/jquery-3.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../css/new_email.css" type="text/css">
</head>
<body>
<div class="a">提交您的意见</div>
<div class="b">
    <form action="/main/puttoemail" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>标题:</label><input type="text" name="title" class="form-control"><br>
        </div>
        <div class="form-group">
            <label>内容:</label><input type="text" name="con" class="form-control"><br>
        </div>
        <div class="form-group">
            <label>姓名:</label><input type="text" name="name" class="form-control"><br>
        </div>
        <div class="form-group">
            <label>邮箱:</label><input type="text" name="email" class="form-control"><br>
        </div>
        <input type="submit" class="btn btn-default">
    </form>
</div>
</body>
</html>
