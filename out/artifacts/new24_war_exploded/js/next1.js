var wrap1 = document.querySelector(".wrap1");
var next1 = document.querySelector(".arrow_right1");
var prev1 = document.querySelector(".arrow_left1");
next1.onclick = function () {
    next_pic1();
}
prev1.onclick = function () {
    prev_pic1();
}

function next_pic1 () {
    var newLeft1;
    if(wrap1.style.left === "-1080px"){
        newLeft1 = -360;
    }else{
        newLeft1 = parseInt(wrap1.style.left)-180;
    }
    wrap1.style.left = newLeft1 + "px";
}


function prev_pic1 () {
    var newLeft1;
    if(wrap1.style.left === "0px"){
        newLeft1 = -720;
    }else{
        newLeft1 = parseInt(wrap1.style.left)+180;
    }
    wrap1.style.left = newLeft1 + "px";
}

var timer = null;
function autoPlay1 () {
    timer = setInterval(function () {
        next_pic1();
    },6000);
}
autoPlay1();


var container1 = document.querySelector(".container1");
container1.onmouseenter = function () {
    clearInterval(timer);
}
container1.onmouseleave = function () {
    autoPlay1();    
}
