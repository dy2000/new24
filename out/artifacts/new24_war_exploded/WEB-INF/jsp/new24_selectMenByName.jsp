<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>修改用户信息</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../../css/gl.css" type="text/css">
    <link rel="stylesheet" href="../../css/new_back.css" type="text/css">
  </head>
  <body>

  <div class="wrap">
  </div>
  <div class="nav-main">
    <div class="nav-box">
      <div class="nav">
        <ul class="nav-ul">
          <li><a href="#" class="home"><span>首页</span></a></li>
          <li><a href="#" class="develop"><span>用户管理</span></a></li>
          <li><a href="#" class="wechat"><span>视频管理</span></a></li>
          <li><a href="#" class="case"><span>展品管理</span></a></li>
          <li><a href="#" class="news"><span>日志管理</span></a></li>
          <li><a href="#" class="contact"><span>关于我们</span></a></li>
        </ul>
      </div>
      <div class="nav-slide">
        <div class="nav-slide-o"></div>
        <div class="nav-slide-o">
          <ul>
            <li><a href="${pageContext.request.contextPath}/list"><span>用户查询</span></a></li>
            <li><a href="${pageContext.request.contextPath}/admin/update1"><span>用户修改</span></a></li>
            <li><a href="${pageContext.request.contextPath}/admin/addBtn"><span>用户添加</span></a></li>
          </ul>
        </div>
        <div class="nav-slide-o">
          <ul>
            <li><a href="${pageContext.request.contextPath}/imglist2"><span>视频查询</span></a></li>
            <li><a href="${pageContext.request.contextPath}/video/b1"><span>视频提交</span></a></li>
          </ul>
        </div>
        <div class="nav-slide-o">
          <ul>
            <li><a href="${pageContext.request.contextPath}/imglist1"><span>显示展品</span></a></li>
            <li><a href="${pageContext.request.contextPath}/img/b1"><span>添加展品</span></a></li>
          </ul>
        </div>
        <div class="nav-slide-o">
          <ul>
            <li><a href="${pageContext.request.contextPath}/loglist"><span>查看所有日志</span></a></li>
          </ul>
        </div>
        <div class="nav-slide-o">
          <ul>
            <li><a href="${pageContext.request.contextPath}/emailList1">显示反馈<span></span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script>
  <script type="text/javascript">
    $(function(){
      var thisTime;
      $('.nav-ul li').mouseleave(function(even){
        thisTime	=	setTimeout(thisMouseOut,1000);
      })

      $('.nav-ul li').mouseenter(function(){
        clearTimeout(thisTime);
        var thisUB	=	$('.nav-ul li').index($(this));
        if($.trim($('.nav-slide-o').eq(thisUB).html()) != "")
        {
          $('.nav-slide').addClass('hover');
          $('.nav-slide-o').hide();
          $('.nav-slide-o').eq(thisUB).show();
        }
        else{
          $('.nav-slide').removeClass('hover');
        }

      })

      function thisMouseOut(){
        $('.nav-slide').removeClass('hover');
      }

      $('.nav-slide').mouseenter(function(){
        clearTimeout(thisTime);
        $('.nav-slide').addClass('hover');
      })
      $('.nav-slide').mouseleave(function(){
        $('.nav-slide').removeClass('hover');
      })

    })
  </script>

  <div class="container">
    <div class="row clearfix">
      <div class="col-md-12 column">
        <div class="page-header">
          <h1>
            <small>修改信息</small>
          </h1>
        </div>
      </div>
    </div>

    <form action="${pageContext.request.contextPath}/admin/update" method="post">
      <input type="hidden" name="menId" value="${men.menId}"/>
      用户名称：<input type="text" name="name" value="${men.name}"/>
      用户qq：<input type="text" name="qq" value="${men.qq}"/>
      用户电话：<input type="text" name="telephone" value="${men.telephone}"/>
      用户地址：<input type="text" name="address" value="${men.address}"/>
      用户日期：<input type="text" name="day" value="<fmt:formatDate value="${men.day}" type="date"/>"/>
      <input type="submit" value="提交"/>
    </form>

  </div>
  </body>
</html>