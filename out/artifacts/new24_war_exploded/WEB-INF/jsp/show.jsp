<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>商店</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../../css/show.css" type="text/css">
  </head>
  <body>
    <!--头部-->
    <div class="head">
            <!--可点击logo-->
            <div class="logo">
                <a href="#">
                    <img src="../../images/1.png" class="logo1">
                </a>
            </div>
            <!--横向导航条-->
            <div class="menu">
                <ul>
                    <li><a href="${pageContext.request.contextPath}/main/main1">首页</a></li>
                    <li><a href="${pageContext.request.contextPath}/main/news">新闻</a></li>
                    <li><a href="${pageContext.request.contextPath}/imglist">视频</a></li>
                    <li><a href="${pageContext.request.contextPath}/main/email">展览</a></li>
                    <li><a href="${pageContext.request.contextPath}/main/show">商店</a></li>
                </ul>
            </div>
            <!--搜索框-->
            <div class="searchdiv">
                <form action="" class="search">
                    <input type="text" class="inputs" placeholder="    搜索...">
                    <a href="">
                        <img src="../../images/搜索.png" class="sou">
                    </a>
                  </form>
            </div>
            <!--登录-->
            <div class="logindiv">
                <a href="${pageContext.request.contextPath}/main/login">
                    <img src="../../images/登录用户黑.png" class="login">
                </a>
            </div>
            <!--邮箱-->
            <div class="emaildiv">
                <a href="">
                    <img src="../../images/邮箱.png" class="email">
                </a>
            </div>

    </div>

        <!--主轮播图-->
        <div class="container">
            <div class="wrap" style="left:-1200px;">
                <a href="#"><img src="../../images/news-show1-1.jpg" alt=""></a>
                <a href="#"><img src="../../images/news-show1-2.jpg" alt=""></a>
                <a href="#"><img src="../../images/news-show1-3.jpg" alt=""></a>
                <a href="#"><img src="../../images/news-show1-4.jpg" alt=""></a>
                <a href="#"><img src="../../images/news-show1-5.jpg" alt=""></a>
                <a href="#"><img src="../../images/news-show1-1.jpg" alt=""></a>
                <a href="#"><img src="../../images/news-show1-2.jpg" alt=""></a>
            </div>
            <a href="javascript:;" class="arrow arrow_left">&lt;</a>
            <a href="javascript:;" class="arrow arrow_right">&gt;</a>
        </div>
        <!--介绍和展览部分-->
    <div class="body1">
        <div class="interduce">
            <p>&nbsp;&nbsp;本博物馆倡导践行展览是最重要的服务产品、策展能力是核心能力等理念，着力打造包含基本陈列、专题展览和临时展览的新展览体系。基本陈列包括“古代中国”“复兴之路”“复兴之路·新时代部分”。专题展览立足馆藏，“中国古代钱币”“中国古代玉器艺术”“中国古代瓷器艺术”等正在展出。遵循“不求所藏、但求所展、开放合作、互利共赢”的原则，临时展览逐步形成主题展览、精品文物展、历史文化展、考古发现展、科技创新展、地域文化展、经典美术展、国际交流展等展览系列。2018年，圆满完成中央交办的“复兴之路·新时代部分”“真理的力量——纪念马克思诞辰200周年主题展览”“伟大的变革——庆祝改革开放40周年大型主题展览”三个重大主题展览，引起强烈反响。</p>
        </div>
        <!-- 图片展览部分 -->
        <div class="container-fluid show-container1">
            <div class="row title1">古中国文明</div>
            <div class="row show-div2">
                <div class="col-md-1.5"></div>
                <div class="col-md-3 show-div1"><a href=""><img src="../../images/news-show2-1.jpg"/></a></div>
                <div class="col-md-3 show-div1"><a href=""><img src="../../images/news-show2-2.jpg"/></a></div>
                <div class="col-md-3 show-div1"><a href=""><img src="../../images/news-show2-3.jpg"/></a></div>
                <div class="col-md-1.5"></div>
            </div>

            <div class="row show-div3">
                <div class="col-md-1.5"></div>
                <div class="col-md-3 show-div1"><br><p><strong>（公元前221年-公元220年）</strong>秦汉时期，长期以来诸侯割据纷争的局面结束，专制主义中央集权制在全国范围内建立，中国历史进入大一统时代。新工艺技术的发明和应用，加速了社会经济的发展，丰富了人们的生活，中外文化交流也空前繁荣。我们的祖先在秦汉时期以其卓越的创造力，建树了中国古代文明发展史上的无数丰碑。</p></div>
                <div class="col-md-3 show-div1"><br><p><strong>（公元220年-589年）</strong>三国两晋南北朝时期，社会动荡，政权更迭频繁，民族融合加强。战争导致大量人口南迁，促进了南方经济发展。瓷器制造成就显著，青瓷制作技术精湛。社会生活形态发生重要变化，家具、服装等出现新形式。社会的变革和中外文化的交融，使思想文化呈现全新面貌。</p></div>
                <div class="col-md-3 show-div1"><br><p><strong>（公元581-960年）</strong>隋唐五代时期，中国历史进入到一个全面繁荣的新阶段。无论是隋唐前期的鼎盛局面，还是唐朝后期至五代十国的社会变革和发展转折，其宏大的格局、开放的气势、壮阔的场面，均为前代所无法比拟。</p></div>
                <div class="col-md-1.5"></div>
            </div>
        </div>

        <br>
        <br>

        <div class="container-fluid show-container1">
            <div class="row title1">钱币之路</div>
            <div class="row show-div2">
                <div class="col-md-1.5"></div>
                <div class="col-md-3 show-div1"><a href=""><img src="../../images/news-show3-1.jpg"/></a></div>
                <div class="col-md-3 show-div1"><a href=""><img src="../../images/news-show3-2.jpg"/></a></div>
                <div class="col-md-3 show-div1"><a href=""><img src="../../images/news-show3-3.jpg"/></a></div>
                <div class="col-md-1.5"></div>
            </div>

            <div class="row show-div3">
                <div class="col-md-1.5"></div>
                <div class="col-md-3 show-div1"><br><p><strong>半两&nbsp;秦</strong><br>半两钱始铸于战国时期的秦国，不晚于秦昭王元年(前306年)。当时的秦国就已经建立了以铢、两为单位的货币制度。公元221年，秦始皇统一中国，废止了战国时期形制不一的旧币，在全国范围内统一推行以两、铢为单位的纪重货币制度。</p></div>
                <div class="col-md-3 show-div1"><br><p><strong>上林三官五铢(武帝)<br>西汉元鼎四年</strong><br>西汉承秦制，铜铸币以两、铢为单位，继续实行纪重货币制度。西汉前期曾多次改变钱币的法定重量，西汉武帝统治期间，先后铸造三铢、四铢半两等重量的钱币。西汉武帝元狩五年（前118年)实行币制改革，始铸五铢钱。</p></div>
                <div class="col-md-3 show-div1"><br><p><strong>原始布币<br>西周<br></strong><br>商周时期，农具铲被称为“钱”，而“布”是当时流通的一种钱币的名称。新朝王莽托古改制，称农具铲形钱币为“布”，为后世沿用。这枚布币较多地保留着农具铲的原型，体形大，铲柄孔尚存，处在实用物品向钱币演化之中。</p></div>
                <div class="col-md-1.5"></div>
            </div>
        </div>
    </div>    
    <br>
    <br>
        <script src="../../js/next.js"></script>
        <script src="../../js/next1.js"></script>
  </body>
</html>