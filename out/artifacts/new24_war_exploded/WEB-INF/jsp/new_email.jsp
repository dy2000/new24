<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>展览</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../js/jquery-3.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../css/vido.css" type="text/css">
    <!-- FONTAWESOME STYLE CSS -->
    <link href="../../css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <link href="../../css/style.css" rel="stylesheet" />
    <link href="../../css/test.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<!--头部-->
<div class="head">
    <!--可点击logo-->
    <div class="logo">
        <a href="#">
            <img src="../../images/1.png" class="logo1">
        </a>
    </div>
    <!--横向导航条-->
    <div class="menu">
        <ul>
            <li><a href="${pageContext.request.contextPath}/main/main1">首页</a></li>
            <li><a href="${pageContext.request.contextPath}/main/news">新闻</a></li>
            <li><a href="${pageContext.request.contextPath}/imglist">视频</a></li>
            <li><a href="${pageContext.request.contextPath}/main/email">展览</a></li>
            <li><a href="${pageContext.request.contextPath}/main/show">商店</a></li>
        </ul>
    </div>
    <!--搜索框-->
    <div class="searchdiv">
        <form action="" class="search">
            <input type="text" class="inputs" placeholder="    搜索...">
            <a href="">
                <img src="../../images/搜索.png" class="sou">
            </a>
        </form>
    </div>
    <!--登录-->
    <div class="logindiv">
        <a href="">
            <img src="../../images/登录用户黑.png" class="login">
        </a>
    </div>
    <!--邮箱-->
    <div class="emaildiv">
        <a href="">
            <img src="../../images/邮箱.png" class="email">
        </a>
    </div>



    <div class="new_div1">
        <!--SPONSORS SECTION END-->
        <hr />
        <section  >
            <div class="container">
                <div class="row">
                    <div class="col-md-9" >
                        <div class="blog-main">
                            <div class="heading-blog">
                                博物馆关于征集相关文物藏品的公告
                            </div>
                            <img src="../../images/news-pitch-1.jpg" class="img-responsive img-rounded" />
                            <div class="blog-info">
                                <a href="${pageContext.request.contextPath}/main/putemail"><span class="label label-primary">提交意见</span></a>
                                <span class="label label-success">联系方式</span>
                            </div>
                            <div class="blog-txt">
                                文物承载灿烂文明，传承历史文化，维系民族精神，是加强社会主义精神文明建设的深厚滋养，国家博物馆是代表国家征集、收藏、保管、展示反映中华优秀传统文化、革命文化和社会主义先进文化代表性物证的最高机构。为进一步充实馆藏，丰富藏品类型形态，珍藏民族集体记忆，保存国家文化基因，为举办重大陈列展览奠定坚实文物基础，促进中外文明交流互鉴，经研究，中国国家博物馆决定面向全社会公开征集藏品，特此公告如下：
                            </div>
                            <div class="blog-txt">
                                为进一步促进博物馆事业的发展、发挥博物馆的功能、规范对博物馆的
                                管理，可以采取哪些制度、措施。
                            </div>
                            <div class="blog-txt">
                                为便于博物馆功能的发挥和政府对其统一规范管理，征求意见稿将博物
                                馆界定为包括博物馆、纪念馆、美术馆、科技馆、陈列馆等向公众开放的非营利,和管理等设定了行政许可，有些行政许可规定了准予许可的条件，有的又没有规
                                定条件。这些规定是否科学、合理。
                            </div>
                        </div>
                        <!--BLOG MAIN SECTION END-->

                        <h3 ><strong>感恩捐赠</strong></h3>
                        <hr />
                        <ul class="media-list">
                            <li class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object img-circle"   src="../../images/news-pitch-2.jpg" />
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">“让好作品有个好归宿”</h4>
                                    <p>
                                        张书旂家属向中国国家博物馆捐赠作品记事
                                        <br>
                                        2020-11-24     博物馆
                                    </p>
                                </div>
                            </li>
                            <!-- COMMENT SECTION - ONE END-->

                            <!-- COMMENT SECTION - THREE END-->
                        </ul>
                        <!--COMMENTS SECTION END-->
                </div>
                </div>
            </div>
        </section>
    </div>
</div>
</body>
</html>
