<%--
  Created by IntelliJ IDEA.
  User: dy
  Date: 2021/6/18
  Time: 11:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>新闻</title>
    <meta name="Robots" content="index,follow" />
    <meta name="author" content="Borut Jegrišnik (www.cssmoban.com)" />
    <link rel="stylesheet" type="text/css" href="../../css/new24_news.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../css/prettyPhoto.css" media="screen" />
    <script type="text/javascript" src="../../js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="../../js/core.js"></script>
    <script type="text/javascript" src="../../js/jquery.pngFix.js"></script>
    <script type="text/javascript" src="../../js/jquery.prettyPhoto.js"></script>
</head>
<body>
<!--头部-->
<div class="head">
    <!--可点击logo-->
    <div class="logo">
        <a href="#">
            <img src="../../images/1.png" class="logo1">
        </a>
    </div>
    <!--横向导航条-->
    <div class="menu1">
        <ul>
            <li><a href="${pageContext.request.contextPath}/main/main1">首页</a></li>
            <li><a href="${pageContext.request.contextPath}/main/news">新闻</a></li>
            <li><a href="${pageContext.request.contextPath}/imglist">视频</a></li>
            <li><a href="${pageContext.request.contextPath}/main/email">展览</a></li>
            <li><a href="${pageContext.request.contextPath}/main/show">商店</a></li>
        </ul>
    </div>
    <!--搜索框-->
    <div class="searchdiv">
        <form action="" class="search">
            <input type="text" class="inputs" placeholder="    搜索...">
            <a href="">
                <img src="../../images/搜索.png" class="sou">
            </a>
        </form>
    </div>
    <!--登录-->
    <div class="logindiv">
        <a href="${pageContext.request.contextPath}/main/login">
            <img src="../../images/登录用户黑.png" class="login">
        </a>
    </div>
    <!--邮箱-->
    <div class="emaildiv">
        <a href="">
            <img src="../../images/邮箱.png" class="email">
        </a>
    </div>

</div>

<div id="wrapper">
    <!-- image slider -->
    <div id="pitch">
        <div class="pitch-gallery">
            <div class="pitch-gallery-holder" id="gallery-pitch-holder">
                <div class="pitch-gallery-div">
                    <img src="../../images/news-pitch-1.jpg" alt="Pitch 1" width="980px" height="357px"/>
                    <div class="infoline">一幅优美的水墨画，出自哪位大师的手笔</div>
                </div>
                <div class="pitch-gallery-div">
                    <img src="../../images/news-pitch-2.jpg" alt="Pitch 2" width="980px" height="357px"/>
                    <div class="infoline">这在当时代也是非常时髦的私人汽车</div>
                </div>
                <div class="pitch-gallery-div">
                    <img src="../../images/news-pitch-3.jpg" alt="Pitch 3" width="980px" height="357px"/>
                    <div class="infoline">画上的人都是谁，有寓意着什么呢？</div>
                </div>
                <div class="pitch-gallery-div">
                    <img src="../../images/news-pitch-4.jpg" alt="Pitch 4" width="980px" height="357px"/>
                    <div class="infoline">中国古代君王上朝场景</div>
                </div>
            </div>
        </div>
    </div>

    <!-- main content -->
    <div id="left">
        <h1><a href="inner.html">关于我们</a></h1>
        <p>     本博物馆拥有藏品10余千件(套)，涵盖了从远古时期到当代各个历史阶段社会发展变化不同方面的内容，其中，国家一级文物近60件(套)，具有高度的历史价值、科学价值和艺术价值，全面系统完整地展现中华优秀传统文化、革命文化、社会主义先进文化，是中华文明发展史的典藏宝库。</p>
        <p>     作为线上的博物馆，本博物馆建立了一整套严格的藏品保管的规章制度，形成了博物馆科学严密的藏品保管体系。同时，加大对馆藏藏品整理工作力度，注重发掘藏品自身的内在价值,发挥其在展览陈列中的印证、具象历史的独特作用，本博物馆顺应时代和社会发展的要求，把各类藏品信息适时公布于众，实现社会共享博物馆的藏品资源，提升馆藏文物的认知度和社会价值。</p>
        <a href="" class="read-more"><strong>更多</strong></a>
    </div>

    <!-- sidebar -->
    <div id="right">
        <h1>最新资讯</h1>

        <h2><a href="#">国际博物馆日</a></h2>
        <p class="post-info">5·18国际博物馆日，社会教育部组织专职讲解员向社会公众提供多场次的公益讲解。</p>

        <h2><a href="#">端午飘香</a></h2>
        <p class="post-info">2021文化和自然遗产日的主题是“人民的非遗 人民共享”</p>

        <h2><a href="#">中学生来馆当文化使者</a></h2>
        <p class="post-info">5月18日，北京四中高中部的同学们来到国家博物馆</p>

        <h2><a href="#">00后大学生为留学生讲党史</a></h2>
        <p class="post-info">5月18日下午，国家博物馆志愿服务协会组织“国博—北语”</p>
    </div>

    <div class="x"></div>
    <div class="break"></div>

    <!-- small posts -->
    <div id="feature">
        <div class="small-post">
            <img src="../../images/news-社会讲解.png" alt="Clean and Simple" width="250px"/>
            <h1><a href="#"><strong>社会讲解</strong></a></h1>
            <p>根据中央关于做好常态化疫情防控工作的指示精神，按照长沙市疫情防控工作统一部署，结合工作实际，本国家博物馆于2020年10月1日起实行错峰限流预约开放。</p>
        </div>

        <div class="small-post">
            <img src="../../images/news-外事讲解.png" alt="jQuery Powered" width="250px"/>
            <h1><a href="#"><strong>外事讲解</strong></a></h1>
            <p>承担馆内外外语接待与讲解工作，以及开展面向外籍人士的教育活动。</p>
        </div>

        <div class="small-post last">
            <img src="../../images/news-专家讲解.png" alt="Contuct Us" width="250px"/>
            <h1><a href="#"><strong>专家讲解</strong></a></h1>
            <p>组织我馆专家、策展人面向公众提供多元化讲解和教育活动。</p>
        </div>
        <div class="x"></div>
    </div>

</div>

<!-- footer -->
<div id="footer">
    <p>Copyright &copy; 2021 DY &middot; Design: DY, <a href="http://www.baidu.com/" title="Free CSS Templates">DY.com</a></p>
</div>
</div>

<!-- gallery starter and prettyPhoto starter -->
<script type="text/javascript">
    jGallery('pitch');
    $(document).pngFix();
    $(document).ready(function(){
        $("a[rel^='prettyPhoto']").prettyPhoto();
    });
</script>
<style>
    .copyrights{text-indent:-9999px;height:0;line-height:0;font-size:0;overflow:hidden;}
</style>
<div class="copyrights" id="DY">
    Collect from <a href="http://www.baidu.com/"  title="DY">DY</a>
    <a href="http://cooco.net.cn/" title="DY">DY</a>
</div>
</body>
</html>
