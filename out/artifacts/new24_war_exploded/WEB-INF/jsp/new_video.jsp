<%--
  Created by IntelliJ IDEA.
  User: dy
  Date: 2021/6/19
  Time: 20:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<title>视频</title>

<!-- Bootstrap -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../../js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../../css/vido.css" type="text/css">
</head>
<body>

<!--头部-->
<div class="head">
    <!--可点击logo-->
    <div class="logo">
        <a href="#">
            <img src="../../images/1.png" class="logo1">
        </a>
    </div>
    <!--横向导航条-->
    <div class="menu">
        <ul>
            <li><a href="${pageContext.request.contextPath}/main/main1">首页</a></li>
            <li><a href="${pageContext.request.contextPath}/main/news">新闻</a></li>
            <li><a href="${pageContext.request.contextPath}/imglist">视频</a></li>
            <li><a href="${pageContext.request.contextPath}/main/email">展览</a></li>
            <li><a href="${pageContext.request.contextPath}/main/show">商店</a></li>
        </ul>
    </div>
    <!--搜索框-->
    <div class="searchdiv">
        <form action="" class="search">
            <input type="text" class="inputs" placeholder="    搜索...">
            <a href="">
                <img src="../../images/搜索.png" class="sou">
            </a>
        </form>
    </div>
    <!--登录-->
    <div class="logindiv">
        <a href="">
            <img src="../../images/登录用户黑.png" class="login">
        </a>
    </div>
    <!--邮箱-->
    <div class="emaildiv">
        <a href="">
            <img src="../../images/邮箱.png" class="email">
        </a>
    </div>

</div>

<iframe frameborder="0" src="https://v.qq.com/txp/iframe/player.html?vid=z00391jxbaw" allowFullScreen="true"class="if1"></iframe>

<div class="body1">

    <div class="listbody2"></div>
    <!--list1 body-->
    <div class="listbody1">
        <ul>
            <li><a title="习近平参观庆祝改革开放40周年大型展览" href="#">习近平参观庆祝改革开放40周年大型展览</a></li>
            <li><a title="习近平：在庆祝中国共产党成立95周年大会上的讲话" href="#">习近平：在庆祝中国共产党成立95周年大会上的讲话</a></li>
            <li><a title="习近平：在党史学习教育动员大会上的讲话" href="#">习近平：在党史学习教育动员大会上的讲话</a></li>
            <li><a title="习近平：切实把革命文物保护好管理好运用好 激发广大干部群众的精神力量" href="#">习近平：切实把革命文物保护好管理好运用好 激发广大干部群众的精</a></li>
            <li><a title="文化和旅游部党组理论学习中心组参观《复兴之路》基本陈列召开党史学习教育专题学习会议" href="#">文化和旅游部党组理论学习中心组参观《复兴之路》基本陈列召开党史    </a></li>
            <li><a title="让历史照亮未来——跟着习近平总书记的足迹学习党史" href="#">让历史照亮未来——跟着习近平总书记的足迹学习党史</a></li>
        </ul>
    </div>

    <table width="200" border="1" class="table table-striped" id="fentable1">
        <c:forEach items="${v.list}" var="user">
            <tr>
                <td><a href="#"><img id="images" alt="" src="${user.img}" class="new_image1"></a></td>
                <td><iframe frameborder="0" src="${user.video}" allowFullScreen="true"class="if2" width="300px" height="300px"></iframe>
                    <div class="infoline">${user.newname}</div>
                </td>
            </tr>
        </c:forEach>
    </table>

</div>

<div class="guide1" id="fendiv1">
    <p>当前 ${v.pageNum }页,总${v.pages }
        页,总 ${v.total } 条记录</p>
    <div class="guide">
        <a href="imglist?pageNo=${v.firstPage}">第一页</a>
        <c:if test="${v.hasPreviousPage }">
            <a href="imglist?pageNo=${v.pageNum-1}">上一页</a>
        </c:if>

        <c:if test="${v.hasNextPage}">
            <a href="imglist?pageNo=${v.pageNum+1}">下一页</a>
        </c:if>
        <a href="imglist?pageNo=${v.lastPage}">最后页</a>
    </div>
</div>
</body>
</html>
