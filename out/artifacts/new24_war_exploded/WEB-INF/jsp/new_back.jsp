<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<title>管理</title>
	<link rel="stylesheet" type="text/css" href="../../css/new_back.css">
</head>
<body>

<div class="wrap">
</div>
<div class="nav-main">
<div class="nav-box">
<div class="nav">
  <ul class="nav-ul">
  	<li><a href="#" class="home"><span>首页</span></a></li>
  	<li><a href="#" class="develop"><span>用户管理</span></a></li>
  	<li><a href="#" class="wechat"><span>视频管理</span></a></li>
  	<li><a href="#" class="case"><span>展品管理</span></a></li>
  	<li><a href="#" class="news"><span>日志管理</span></a></li>
  	<li><a href="#" class="contact"><span>关于我们</span></a></li>
  </ul>
</div>
<div class="nav-slide">
    <div class="nav-slide-o"></div>
    <div class="nav-slide-o">
    	<ul>
    		<li><a href="${pageContext.request.contextPath}/list"><span>用户查询</span></a></li>
    		<li><a href="${pageContext.request.contextPath}/admin/update1"><span>用户修改</span></a></li>
    		<li><a href="${pageContext.request.contextPath}/admin/addBtn"><span>用户添加</span></a></li>
    	</ul>
    </div>
    <div class="nav-slide-o">
    	<ul>
    		<li><a href="${pageContext.request.contextPath}/imglist2"><span>视频查询</span></a></li>
    		<li><a href="${pageContext.request.contextPath}/video/b1"><span>视频提交</span></a></li>
    	</ul>
    </div>
    <div class="nav-slide-o">
    	<ul>
			<li><a href="${pageContext.request.contextPath}/imglist1"><span>显示展品</span></a></li>
			<li><a href="${pageContext.request.contextPath}/img/b1"><span>添加展品</span></a></li>
    	</ul>
    </div>
    <div class="nav-slide-o">
    	<ul>
			<li><a href="${pageContext.request.contextPath}/loglist"><span>查看所有日志</span></a></li>
    	</ul>
    </div>
	<div class="nav-slide-o">
		<ul>
			<li><a href="${pageContext.request.contextPath}/emailList1">显示反馈<span></span></a></li>
		</ul>
	</div>
</div>
</div>
</div>

<script type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
	$(function(){
	var thisTime;
	$('.nav-ul li').mouseleave(function(even){
			thisTime	=	setTimeout(thisMouseOut,1000);
	})

	$('.nav-ul li').mouseenter(function(){
		clearTimeout(thisTime);
		var thisUB	=	$('.nav-ul li').index($(this));
		if($.trim($('.nav-slide-o').eq(thisUB).html()) != "")
		{
			$('.nav-slide').addClass('hover');
			$('.nav-slide-o').hide();
			$('.nav-slide-o').eq(thisUB).show();
		}
		else{
			$('.nav-slide').removeClass('hover');
		}
		
	})
	
	function thisMouseOut(){
		$('.nav-slide').removeClass('hover');
	}
	 
	$('.nav-slide').mouseenter(function(){
		clearTimeout(thisTime);
		$('.nav-slide').addClass('hover');
	})
	$('.nav-slide').mouseleave(function(){
		$('.nav-slide').removeClass('hover');
	})

})
</script>

</body>
</html>