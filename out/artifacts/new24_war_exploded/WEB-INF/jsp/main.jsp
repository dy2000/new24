<%
    String path=request.getContextPath();
    String basePath=request.getScheme()+"://"+request.getServerName()+":"
    +request.getServerPort()+path+"/";
%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <base href="<%=basePath%>>">
        <title>狐狸博物馆</title>
        <link rel="stylesheet" type="text/css" href="/css/main.css">
    </head>
    <body>
        <!--头部-->
    <div class="head">
            <!--可点击logo-->
            <div class="logo">
                <a href="#">
                    <img src="../../images/1.png" class="logo1">
                </a>
            </div>
            <!--横向导航条-->
            <div class="menu">
                <ul>
                    <li><a href="${pageContext.request.contextPath}/main/main1">首页</a></li>
                    <li><a href="${pageContext.request.contextPath}/main/news">新闻</a></li>
                    <li><a href="${pageContext.request.contextPath}/imglist">视频</a></li>
                    <li><a href="${pageContext.request.contextPath}/main/email">展览</a></li>
                    <li><a href="${pageContext.request.contextPath}/main/show">商店</a></li>
                </ul>
            </div>
            <!--搜索框-->
            <div class="searchdiv">
                <form action="" class="search">
                    <input type="text" class="inputs" placeholder="    搜索...">
                    <a href="">
                        <img src="../../images/搜索.png" class="sou">
                    </a>
                  </form>
            </div>
            <!--登录-->
            <div class="logindiv">
                <a href="${pageContext.request.contextPath}/main/login">
                    <img src="../../images/登录用户黑.png" class="login">
                </a>
            </div>
            <!--邮箱-->
            <div class="emaildiv">
                <a href="">
                    <img src="../../images/邮箱.png" class="email">
                </a>
            </div>

    </div>

        <!--主轮播图-->
        <div class="container">
            <div class="wrap" style="left:-1200px;">
                <a href="#"><img src="${pageContext.request.contextPath}/images/第一面国旗.png" alt=""></a>
                <a href="#"><img src="${pageContext.request.contextPath}/images/人面鱼纹彩陶盆.jpg" alt=""></a>
                <a href="#"><img src="${pageContext.request.contextPath}/images/后母戊鼎.png" alt=""></a>
                <a href="#"><img src="${pageContext.request.contextPath}/images/木追.png" alt=""></a>
                <a href="#"><img src="${pageContext.request.contextPath}/images/李宁.jpg" alt=""></a>
                <a href="#"><img src="${pageContext.request.contextPath}/images/第一面国旗.png" alt=""></a>
                <a href="#"><img src="${pageContext.request.contextPath}/images/人面鱼纹彩陶盆.jpg" alt=""></a>
            </div>
            <a href="javascript:;" class="arrow arrow_left">&lt;</a>
            <a href="javascript:;" class="arrow arrow_right">&gt;</a>
        </div>

            <!--横图-->
        <div id="line">

            <!--时间-->
            <span id="time"></span>
            
            <!--横向滚动条-->
            <div id="scroll_div" class="fl"> 
                <div id="scroll_begin">
                    “中国大运河史诗图卷”在国博展出 <span class="pad_right">     2021-04-13    中国博物馆</span>
                    习近平参观庆祝改革开放40周年大型展览 <span class="pad_right">       2018-11-13    新华社</span>
                    习近平：在庆祝中国共产党成立95周年大会上的讲话 <span class="pad_right">     2021-04-15    新华社</span>
                    习近平：在党史学习教育动员大会上的讲话 <span class="pad_right">     2021-03-31    《求是》</span>
                    习近平：切实把革命文物保护好管理好运用好 激发广大干部群众的精神力量 <span class="pad_right">        2021-03-30    新华社</span>
                    文化和旅游部党组理论学习中心组参观《复兴之路》基本陈列召开党史学习教育专题学习会议 <span class="pad_right">       2021-04-14    文化和旅游部政府门户网站</span>
                    让历史照亮未来——跟着习近平总书记的足迹学习党史 <span class="pad_right">     2021-03-27    人民日报海外版</span>
                </div> 
                <div id="scroll_end"></div>
           </div>
           <!--更多-->
            <div>
               <a href="#"><img src="/images/更多.png" class="more"></a>
            </div> 
        </div>

            <!--list区-->
        <div class="list">

          <div class="list1">
            <!--list1 head-->
            <div class="l1">
                <div class="head1">时事要闻</div>
                <div>
                    <a href="#">
                        <img src="/images/更多.png" class="more1">
                    </a>
                </div>
            </div>
            <!--list1 body-->
            <div class="listbody1"> 
                <ul>
                    <li><a title="习近平参观庆祝改革开放40周年大型展览" href="#">习近平参观庆祝改革开放40周年大型展览</a></li>
                    <li><a title="习近平：在庆祝中国共产党成立95周年大会上的讲话" href="#">习近平：在庆祝中国共产党成立95周年大会上的讲话</a></li>
                    <li><a title="习近平：在党史学习教育动员大会上的讲话" href="#">习近平：在党史学习教育动员大会上的讲话</a></li>
                    <li><a title="习近平：切实把革命文物保护好管理好运用好 激发广大干部群众的精神力量" href="#">习近平：切实把革命文物保护好管理好运用好 激发广大干部群众的精</a></li>
                    <li><a title="文化和旅游部党组理论学习中心组参观《复兴之路》基本陈列召开党史学习教育专题学习会议" href="#">文化和旅游部党组理论学习中心组参观《复兴之路》基本陈列召开党史    </a></li>
                    <li><a title="让历史照亮未来——跟着习近平总书记的足迹学习党史" href="#">让历史照亮未来——跟着习近平总书记的足迹学习党史</a></li>
                </ul>
            </div>

          </div>

          <div class="list2">
            <!--list2 head-->
            <div class="l2">
                <div class="head2">狐狸新事</div>
                <div>
                    <a href="#">
                        <img src="/images/更多.png" class="more2">
                    </a>
                </div>
            </div>
            <!--list2 body-->
            <div class="listbody2"> 
                <ul>
                    <li><a title="建中国特色考古学 办国博品质考古展——国博用行动积极贯彻落实习近平总书记关于考古工作重要讲话精神" href="#">建中国特色考古学 办国博品质考古展——国博用行动积极贯彻落实习</a></li>
                    <li><a title="习近平：在庆祝中国共产党成立95周年大会上的讲话" href="#">习近平：在庆祝中国共产党成立95周年大会上的讲话</a></li>
                    <li><a title="习近平：在党史学习教育动员大会上的讲话" href="#">习近平：在党史学习教育动员大会上的讲话</a></li>
                    <li><a title="习近平：切实把革命文物保护好管理好运用好 激发广大干部群众的精神力量" href="#">习近平：切实把革命文物保护好管理好运用好 激发广大干部群众的精</a></li>
                    <li><a title="文化和旅游部党组理论学习中心组参观《复兴之路》基本陈列召开党史学习教育专题学习会议" href="#">文化和旅游部党组理论学习中心组参观《复兴之路》基本陈列召开党史    </a></li>
                    <li><a title="让历史照亮未来——跟着习近平总书记的足迹学习党史" href="#">让历史照亮未来——跟着习近平总书记的足迹学习党史</a></li>
                </ul>
            </div>

          </div>

        </div>
        <!--小轮播图-->
        <div>
            <div class="container1">
                <div class="wrap1" style="left:-180px;">
                    <a href="#"><img src="/images/第一面国旗.png" alt=""></a>
                    <a href="#"><img src="/images/人面鱼纹彩陶盆.jpg" alt=""></a>
                    <a href="#"><img src="/images/后母戊鼎.png" alt=""></a>
                    <a href="#"><img src="/images/木追.png" alt=""></a>
                    <a href="#"><img src="/images/李宁.jpg" alt=""></a>
                    <a href="#"><img src="/images/第一面国旗.png" alt=""></a>
                    <a href="#"><img src="/images/人面鱼纹彩陶盆.jpg" alt=""></a>
                </div>
                <a href="javascript:;" class="arrow arrow_left1">&lt;</a>
                <a href="javascript:;" class="arrow arrow_right1">&gt;</a>
            </div>
        </div>


        <script src="/js/line.js"></script>
        <script src="/js/time.js"></script>
        <script src="/js/next.js"></script>
        <script src="/js/next1.js"></script>
    </body>
</html>