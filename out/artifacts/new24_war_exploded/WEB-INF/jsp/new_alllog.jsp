<%--
  Created by IntelliJ IDEA.
  User: dy
  Date: 2021/6/22
  Time: 14:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>日志详情</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../js/jquery-3.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../css/fen.css" type="text/css">
    <link rel="stylesheet" href="../../css/new_back.css" type="text/css">
</head>
<body>
<center>


    <div class="wrap">
    </div>
    <div class="nav-main">
        <div class="nav-box">
            <div class="nav">
                <ul class="nav-ul">
                    <li><a href="#" class="home"><span>首页</span></a></li>
                    <li><a href="#" class="develop"><span>用户管理</span></a></li>
                    <li><a href="#" class="wechat"><span>视频管理</span></a></li>
                    <li><a href="#" class="case"><span>展品管理</span></a></li>
                    <li><a href="#" class="news"><span>日志管理</span></a></li>
                    <li><a href="#" class="contact"><span>关于我们</span></a></li>
                </ul>
            </div>
            <div class="nav-slide">
                <div class="nav-slide-o"></div>
                <div class="nav-slide-o">
                    <ul>
                        <li><a href="${pageContext.request.contextPath}/list"><span>用户查询</span></a></li>
                        <li><a href="${pageContext.request.contextPath}/admin/update1"><span>用户修改</span></a></li>
                        <li><a href="${pageContext.request.contextPath}/admin/addBtn"><span>用户添加</span></a></li>
                    </ul>
                </div>
                <div class="nav-slide-o">
                    <ul>
                        <li><a href="${pageContext.request.contextPath}/imglist2"><span>视频查询</span></a></li>
                        <li><a href="${pageContext.request.contextPath}/video/b1"><span>视频提交</span></a></li>
                    </ul>
                </div>
                <div class="nav-slide-o">
                    <ul>
                        <li><a href="${pageContext.request.contextPath}/imglist1"><span>显示展品</span></a></li>
                        <li><a href="${pageContext.request.contextPath}/img/b1"><span>添加展品</span></a></li>
                    </ul>
                </div>
                <div class="nav-slide-o">
                    <ul>
                        <li><a href="${pageContext.request.contextPath}/loglist"><span>查看所有日志</span></a></li>
                    </ul>
                </div>
                <div class="nav-slide-o">
                    <ul>
                        <li><a href="${pageContext.request.contextPath}/emailList1">显示反馈<span></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
        $(function(){
            var thisTime;
            $('.nav-ul li').mouseleave(function(even){
                thisTime	=	setTimeout(thisMouseOut,1000);
            })

            $('.nav-ul li').mouseenter(function(){
                clearTimeout(thisTime);
                var thisUB	=	$('.nav-ul li').index($(this));
                if($.trim($('.nav-slide-o').eq(thisUB).html()) != "")
                {
                    $('.nav-slide').addClass('hover');
                    $('.nav-slide-o').hide();
                    $('.nav-slide-o').eq(thisUB).show();
                }
                else{
                    $('.nav-slide').removeClass('hover');
                }

            })

            function thisMouseOut(){
                $('.nav-slide').removeClass('hover');
            }

            $('.nav-slide').mouseenter(function(){
                clearTimeout(thisTime);
                $('.nav-slide').addClass('hover');
            })
            $('.nav-slide').mouseleave(function(){
                $('.nav-slide').removeClass('hover');
            })

        })
    </script>

    <a href="${pageContext.request.contextPath}/main/out" rel="external nofollow" >导出</a>
    <table width="200" border="1" class="table table-striped" id="fentable1">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">日志执行人</th>
            <th scope="col">执行操作</th>
            <th scope="col">执行时间</th>
        </tr>
        <c:forEach items="${g.list}" var="v">
            <tr>
                <td>${v.id}</td>
                <td>${v.name}</td>
                <td>${v.method}</td>
                <td>${v.time}</td>
            </tr>
        </c:forEach>
    </table>
    <div class="" id="fendiv1">
        <p>当前 ${g.pageNum }页,总${g.pages }
            页,总 ${g.total } 条记录</p>
        <a href="loglist?pageNo=${g.firstPage}">第一页</a>
        <c:if test="${g.hasPreviousPage }">
            <a href="loglist?pageNo=${g.pageNum-1}">上一页</a>
        </c:if>

        <c:if test="${g.hasNextPage}">
            <a href="loglist?pageNo=${g.pageNum+1}">下一页</a>
        </c:if>
        <a href="loglist?pageNo=${g.lastPage}">最后页</a>
    </div>
</center>
</body>
</html>
