<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fm" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
    String path =request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<base href="<%=basePath%>>">
<html>
    <head>
        <meta charset="UTF-8">
        <title>登录界面</title>
        <link rel="stylesheet" type="text/css" href="/css/login.css">
    </head>
    <body>
        <fm:form action="user/login" method="post" id="login1">
            <h1 class="hello">欢迎登陆</h1>
            <div id="user"><label for="username">用户名:</label> <input type="text" id="username" name="username" placeholder="             请输入用户名"> <fm:errors path="username"/> </div>
            <div id="pwd"><label for="password" id="ma">密码:</label> <input type="password" name="password" placeholder="             请输入密码" id="password"> <fm:errors path="password"/> </div>
            <div id="but"><input type="submit" value="" id="buto"></div>
        </fm:form>
        <form action="user/btn1" method="post" id="guanli">
            <input type="submit" value="" id="btn2">
        </form>
        <img src="/images/猫咪.png" class="mi" id="but1" title="猫咪">
    </body>
</html>