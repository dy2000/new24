/*
Navicat MySQL Data Transfer

Source Server         : demo
Source Server Version : 50562
Source Host           : localhost:3308
Source Database       : a

Target Server Type    : MYSQL
Target Server Version : 50562
File Encoding         : 65001

Date: 2021-06-23 11:56:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for control
-- ----------------------------
DROP TABLE IF EXISTS `control`;
CREATE TABLE `control` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(255) NOT NULL,
  `pwd` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control
-- ----------------------------
INSERT INTO `control` VALUES ('1', '邓煜', '123');
INSERT INTO `control` VALUES ('2', '綦帅', '123');
INSERT INTO `control` VALUES ('3', '费东亮', '123');
INSERT INTO `control` VALUES ('4', '李维韬', '123');

-- ----------------------------
-- Table structure for email
-- ----------------------------
DROP TABLE IF EXISTS `email`;
CREATE TABLE `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `con` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of email
-- ----------------------------
INSERT INTO `email` VALUES ('1', '好评', '请给这些页面加些动画', '綦帅', '1241431@qq.com');
INSERT INTO `email` VALUES ('2', '好评', '内容还不错', '綦帅', '12124332');
INSERT INTO `email` VALUES ('3', '好评', '请给这些页面加些动画', '綦帅', '1241431@qq.com');
INSERT INTO `email` VALUES ('4', '差评', '还没完成', '李维韬', '12413');
INSERT INTO `email` VALUES ('5', '好评', '请给这些页面加些动画', '綦帅', '1241431@qq.com');
INSERT INTO `email` VALUES ('6', '差评', '还没完成', '李维韬', '1241431@qq.com');

-- ----------------------------
-- Table structure for img
-- ----------------------------
DROP TABLE IF EXISTS `img`;
CREATE TABLE `img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of img
-- ----------------------------
INSERT INTO `img` VALUES ('2', '/images/71779dc8adaf438d8ff4aa7054b6d6db.jpeg', '古埃及');
INSERT INTO `img` VALUES ('3', '/images/619866c22028455b9d4e65eb82301fe0.jpeg', '文物');
INSERT INTO `img` VALUES ('4', '/images/b14f2ec390e5430797ed0b2e7e1c5c1f.jpeg', '文物');
INSERT INTO `img` VALUES ('5', '/images/62d85b12c8f04239a88171007ea3e25e.jpeg', '文物');
INSERT INTO `img` VALUES ('6', '/images/a1da3511e20b434db447e68eb89614b2.jpeg', '文物');
INSERT INTO `img` VALUES ('7', '/images/9f86ba843c434de4acdf845f6505dbe6.png', '热点');
INSERT INTO `img` VALUES ('8', '/images/84b39170533a4623a186dcd42d3b5437.jpeg', '热点');

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `q` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES ('38', 'dy', '登录', '0', '2021-06-22 22:49:52');
INSERT INTO `log` VALUES ('39', 'dy', '登录', '0', '2021-06-22 22:50:17');
INSERT INTO `log` VALUES ('40', 'dy', '登录', '0', '2021-06-22 22:50:56');
INSERT INTO `log` VALUES ('41', 'dy', '登录', '0', '2021-06-22 22:51:52');
INSERT INTO `log` VALUES ('42', 'dy', '登录', '0', '2021-06-22 23:26:38');
INSERT INTO `log` VALUES ('43', 'dy', '登录', '0', '2021-06-22 23:30:03');
INSERT INTO `log` VALUES ('44', 'dy', '登录', '0', '2021-06-22 23:35:09');
INSERT INTO `log` VALUES ('45', 'dy', '登录', '0', '2021-06-22 23:39:48');
INSERT INTO `log` VALUES ('46', 'dy', '登录', '0', '2021-06-22 23:40:47');
INSERT INTO `log` VALUES ('47', 'dy', '登录', '0', '2021-06-22 23:42:10');
INSERT INTO `log` VALUES ('48', 'qs', '登录', '0', '2021-06-22 23:47:20');
INSERT INTO `log` VALUES ('49', 'dy', '登录', '0', '2021-06-22 23:53:02');
INSERT INTO `log` VALUES ('50', 'dy', '登录', '0', '2021-06-22 23:55:38');
INSERT INTO `log` VALUES ('51', 'dy', '登录', '0', '2021-06-23 00:05:47');
INSERT INTO `log` VALUES ('52', 'dy', '登录', '0', '2021-06-23 00:06:31');
INSERT INTO `log` VALUES ('53', 'dy', '登录', '0', '2021-06-23 00:13:49');
INSERT INTO `log` VALUES ('54', '邓煜', '登录', '1', '2021-06-23 00:23:04');
INSERT INTO `log` VALUES ('55', 'dy', '登录', '0', '2021-06-23 00:23:30');
INSERT INTO `log` VALUES ('56', 'dy', '登录', '0', '2021-06-23 10:45:43');
INSERT INTO `log` VALUES ('57', '邓煜', '登录', '1', '2021-06-23 10:48:01');
INSERT INTO `log` VALUES ('58', '邓煜', '上传了视频封面', null, null);
INSERT INTO `log` VALUES ('59', 'dy', '登录', '0', '2021-06-23 10:59:49');
INSERT INTO `log` VALUES ('60', 'dy', '更新', null, '2021-06-23 11:00:29');
INSERT INTO `log` VALUES ('61', '邓煜', '登录', '1', '2021-06-23 11:00:37');
INSERT INTO `log` VALUES ('62', '邓煜', '删除了展览物品图片', null, '2021-06-23 11:01:34');
INSERT INTO `log` VALUES ('63', 'dy', '登录', '0', '2021-06-23 11:07:06');
INSERT INTO `log` VALUES ('64', '邓煜', '登录', '1', '2021-06-23 11:08:46');
INSERT INTO `log` VALUES ('65', '邓煜', '删除了一条建议', null, '2021-06-23 11:08:57');
INSERT INTO `log` VALUES ('66', 'dy', '登录', '0', '2021-06-23 11:50:07');
INSERT INTO `log` VALUES ('67', 'dy', '登录', '0', '2021-06-23 11:50:55');
INSERT INTO `log` VALUES ('68', '邓煜', '登录', '1', '2021-06-23 11:51:07');

-- ----------------------------
-- Table structure for man
-- ----------------------------
DROP TABLE IF EXISTS `man`;
CREATE TABLE `man` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of man
-- ----------------------------
INSERT INTO `man` VALUES ('1', '邓煜');

-- ----------------------------
-- Table structure for menmessage
-- ----------------------------
DROP TABLE IF EXISTS `menmessage`;
CREATE TABLE `menmessage` (
  `menId` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(255) NOT NULL,
  `telephone` char(255) DEFAULT NULL,
  `address` char(255) DEFAULT NULL,
  `qq` int(20) DEFAULT NULL,
  `day` date DEFAULT NULL,
  PRIMARY KEY (`menId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menmessage
-- ----------------------------
INSERT INTO `menmessage` VALUES ('1', 'dy', '17680789469', '阿斯蒂芬事情', '1158674023', '2021-12-24');
INSERT INTO `menmessage` VALUES ('2', 'qs', '123456789', '理工学院', '123412448', '2001-04-10');
INSERT INTO `menmessage` VALUES ('3', 'fdl', '123456789', '理工学院', '1158674023', '2021-04-24');
INSERT INTO `menmessage` VALUES ('4', 'lwt', '21341234', '阿斯蒂芬事情发生', '1234124', '2021-03-24');
INSERT INTO `menmessage` VALUES ('17', '测试1', '123123123', '湖南省', '11231231', '2001-04-10');
INSERT INTO `menmessage` VALUES ('19', '测试2', '12312412', '湖南省', '1158674023', '2021-12-24');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(255) NOT NULL,
  `password` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'dy', '123');
INSERT INTO `user` VALUES ('2', 'qs', '123');
INSERT INTO `user` VALUES ('3', 'fdl', '123');
INSERT INTO `user` VALUES ('4', 'lwt', '123');
INSERT INTO `user` VALUES ('5', '测试1', '123');
INSERT INTO `user` VALUES ('6', '测试2', '123');
INSERT INTO `user` VALUES ('26', '测试1', '123');
INSERT INTO `user` VALUES ('27', '测试2', '123');
INSERT INTO `user` VALUES ('28', '测试2', '123');

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newname` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES ('1', '寻红色文物 悟中国精神|新青年（下）', '热点', '/images/6a04807dd5494d50805dc1d5c79338b7.jpeg', 'https://v.qq.com/txp/iframe/player.html?vid=i3249xr2yc2');
INSERT INTO `video` VALUES ('2', '綦帅坐火车被车撞', '热点', '/images/604e5b1e3f784969ba9149bc916cd01b.jpeg', 'https://v.qq.com/txp/iframe/player.html?vid=m0897pq46k3');
INSERT INTO `video` VALUES ('3', '“龙门遗粹——山西河津窑考古成果展”在中国国家博物馆展出', '热点', '/images/6e3ef142f3cc46398d77d419deff867b.jpeg', 'https://v.qq.com/txp/iframe/player.html?vid=o097290cabp');
INSERT INTO `video` VALUES ('4', '寻红色文物 悟中国精神|新青年（下）', '热点', '/images/69845d9009fb4970a5f4e35a964e8d19.jpeg', 'https://v.qq.com/txp/iframe/player.html?vid=x0364uobinl');
INSERT INTO `video` VALUES ('5', '邓稼先研制第一颗原子弹时身边的“宝贝”是它！丨红色印记第62集', '热点', '/images/8b84b65a21c84efbb175d4aa95fe1968.jpeg', 'https://v.qq.com/txp/iframe/player.html?vid=o0874gwaqxg');
INSERT INTO `video` VALUES ('6', '寻红色文物 悟中国精神|新青年（下）', '热点', '/images/b73ab71b034647e4a88939b0e33f6a54.jpeg', 'https://v.qq.com/txp/iframe/player.html?vid=i3249xr2yc2');
INSERT INTO `video` VALUES ('10', '“龙门遗粹——山西河津窑考古成果展”在中国国家博物馆展出', '热点', '/images/d4b7b6f94f1e4300adfcd8bfff1b0bcf.jpeg', 'https://v.qq.com/txp/iframe/player.html?vid=v3250t0mboq');
